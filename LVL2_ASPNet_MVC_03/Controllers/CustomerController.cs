﻿using LVL2_ASPNet_MVC_03.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LVL2_ASPNet_MVC_03.Controllers
{
    public class CustomerController : Controller
    {
        db_customer4Entities dbModel = new db_customer4Entities();
        // GET: Customer
        public ActionResult Index()
        {
            return View(dbModel.tbl_customer.ToList());
        }

        // GET: Customer/Details/5
        public ActionResult Details(int id)
        {
            return View(dbModel.tbl_customer.Where(x => x.Id == id).FirstOrDefault());
        }

        // GET: Customer/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public ActionResult Create(tbl_customer customer)
        {
            // TODO: Add insert logic here
            /*dbModel.tbl_customer.Add(customer);
            dbModel.SaveChanges();*/
            using (var transactions = dbModel.Database.BeginTransaction()) {
                try
                {
                    //int a = 10;
                    //int b = 0;
                    //int c = a / b;
                    /*
                    tbl_customer add = new tbl_customer
                    {
                        Name = customer.Name,
                        Phone_Number = customer.Phone_Number,
                        Email = customer.Email
                    };
                    dbModel.Entry(add).State = EntityState.Added;
                    dbModel.SaveChanges();
                    */
                    tbl_customer Edit = dbModel.tbl_customer.Where(x => x.Id == 4).FirstOrDefault();
                    Edit.Description = "Jakarta Barat Raya";
                    dbModel.Entry(Edit).State = EntityState.Modified;
                    dbModel.SaveChanges();

                    transactions.Commit();
                    return RedirectToAction("Index");
                }
                catch (Exception msgErr)
                {
                    //ViewBag.Error = msgErr.Message;
                    if (msgErr is DivideByZeroException) {
                        transactions.Rollback();
                        ViewBag.Error = msgErr.Message;
                    }
                    return View();
                }
            }
        }

        // GET: Customer/Edit/5
        public ActionResult Edit(int id)
        {
            return View(dbModel.tbl_customer.Where(x => x.Id == id).FirstOrDefault());
        }

        // POST: Customer/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, tbl_customer customer)
        {
            try
            {
                // TODO: Add update logic here
                dbModel.Entry(customer).State = EntityState.Modified;
                dbModel.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Customer/Delete/5
        public ActionResult Delete(int id)
        {
            return View(dbModel.tbl_customer.Where(x => x.Id == id).FirstOrDefault());
        }

        // POST: Customer/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, tbl_customer customer)
        {
            try
            {
                // TODO: Add delete logic here
                customer = dbModel.tbl_customer.Where(x => x.Id == id).FirstOrDefault();
                dbModel.tbl_customer.Remove(customer);
                dbModel.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
